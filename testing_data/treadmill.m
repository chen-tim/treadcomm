% General Converison
m_to_mi = 0.000621371; % mi
h_to_s = 60^2; % s

% Measured radius - diameter of 3.9 in ~= 10 cm
r = 3.9 / 2 * .0254; % m

% Conversions given nominal radius
hz_to_mps = 2 * pi * r;
mps_to_miph = h_to_s * m_to_mi;

% --> Failed gut check, do quick 'n dirty experiments

%% Experimental Validation
% Checking the travel time of a piece of rubber at different commanded frequencies
d = 2.5; % m
t = 2.1; % s
f = 15; % Hz

% Recomputed "radius"
rn = (d / t) / (2 * pi * f) % m

% Test Input: Frequencies
fs = [5, 10, 12, 15, 18, 20, 25] % Hz

% Time to fly off treadmill
% - Model
ts = d ./ (2 * pi * fs * rn) % s
% - Experiment Measurements
ts_e = [6.23, 3.19, 2.65, 2.1, 1.72, 1.59, 1.3] % s

% Difference in model and experiments
ts - ts_e
