% General Converison
m_to_mi = 0.000621371; % mi
h_to_s = 60^2; % s

% Measured radius - diameter of 5 in
r = 5 / 2 * .0254 % m

% Conversions given nominal radius
hz_to_mps = 2 * pi * r;
mps_to_miph = h_to_s * m_to_mi;

% --> Failed gut check, do quick 'n dirty experiments

%% Experimental Validation
% Checking the travel time of a piece of rubber at different commanded frequencies
d = 4.13385;     % m
t = 9.9833;    % s
f = 2.5;          % Hz

% Recomputed "radius"
rn = (d / t) / (2 * pi * f) % m

% Test Input: Frequencies
fs = [1.0 1.5 2.0 2.5 3.0 3.5 4.0 4.5 6.0 8.0 15.0 20.0] % Hz

% Time to fly off treadmill
% - Model
ts = d ./ (2 * pi * fs * rn) % s
% - Experiment Measurements
ts_e = [24.58, 16.50, 12.3467, 9.9833, 8.2567, 6.8733, 6.10, 5.43, 4.34, 3.04, 1.90, 1.39] % s

% Difference in model and experiments
ts - ts_e

% Conversion Factor for Code
conv = 1/ ( 2 * pi * rn)

% Plot
plot(ts_e,fs); hold on;
plot(ts,fs,'--r');
