#include <tread_comm/tread_comm.h>

/* CONSTRUCTOR
 * @PARAM device - location of the serial device - ex: "/dev/ttyACM0"
 * @PARAM baud - Communication baud rate, eg. 9600, 19200, 57600, 115200
 */
tread_communicator::tread_communicator(const char *device, int baud, bool throw_if_error): devloc(device), baudrate(baud), throw_if_error(throw_if_error) {
    TMC = NULL;
}

/* DESTRUCTOR
 * Close connections and free the device
 */
tread_communicator::~tread_communicator() {
    modbus_close(TMC);
    modbus_free(TMC);
}


/* FUNCTION setFrequency(uint16_t freq)
 * (1) Specifies the frequency reference for the motor and (2) runs the command.
 * @PARAM freq - Frequency in Hz commanded to the motor
 */
int tread_communicator::setFrequency(uint16_t freq) {
  int ret;
  uint16_t full_packet[1];
  full_packet[0] = freq;

  ret = writeTMC(TMC_FREQ_REF, 1, full_packet);

  if (ret == -1) {
    if (throw_if_error)
        throw tread_exception(modbus_strerror(errno));
    else
    {
        fprintf(stderr, "%s\n", modbus_strerror(errno));
        return -1;
    }
  }

  return 0;
}

/* FUNCTION setSpeed(double speed)
 * Converts the forward hip velocity to motor hz and sends the command.
 * @PARAM speed - Linear velocity of robot hip. In meters / second
 */
int tread_communicator::setSpeed(double speed) {
    int ret;
    int hz = 0;
    hz = speed_map(speed);
    //printf("speed: %d\n", hz);
    if (hz > 6 || hz < 1.0)
        ret = setFrequency(hz);
    else {
        ret = setFrequency(0);
        std::cout << "Tried to set speed out of bounds!\t" << hz << "Hz\n";
    }

    if (ret == -1) {
        if (throw_if_error)
            throw tread_exception(modbus_strerror(errno));
        else
        {
            std::cout << "Could not set speed!" << modbus_strerror(errno) << "\n";
            return -1;
        }
    }
    return 0;
}

/* FUNCTION getFrequency(void)
 * (1) Gets frequency feedback from the motor.
 * @RETURN Data read from register.
 */
uint16_t tread_communicator::getFrequency() {
  int ret;
  uint16_t data[32];

  ret = readTMC(TMC_FREQ_REFERENCE_READ, 1, data);


  if (ret == -1) {
      if (throw_if_error)
          throw tread_exception(modbus_strerror(errno));
      else
      {
        fprintf(stderr, "%s\n", modbus_strerror(errno));
        return -1;
      }
  }

  return data[0];
}

double tread_communicator::getSpeed()
{
    uint16_t freq = getFrequency();
    return speed_map_inverse(freq);
}

int tread_communicator::startup()
{
    uint16_t startup[1] = {0x09};
    int ret = writeTMC(TMC_MULTI_FUNCTION_INPUTS, 1, startup); // Start up motor

    if (ret == -1) {
        if (throw_if_error)
            throw tread_exception(modbus_strerror(errno));
        else
        {
          fprintf(stderr, "%s\n", modbus_strerror(errno));
          return -1;
        }
    }
}

int tread_communicator::stop()
{
    uint16_t startup[1] = {0x00};
    int ret = writeTMC(TMC_MULTI_FUNCTION_INPUTS, 1, startup); // Shut Down motor

    if (ret == -1) {
        if (throw_if_error)
            throw tread_exception(modbus_strerror(errno));
        else
        {
          fprintf(stderr, "%s\n", modbus_strerror(errno));
          return -1;
        }
    }
}

/* FUNCTION writeTMC(TMC_Registers reg, uint16_t *data)
 * Write a byte to a register
 * @PARAM reg - Register to write the data to
 * @PARAM data - Unsigned integer data to be written to register
 * @RETURN int = -1 if error, 0 if ok
 */
int tread_communicator::writeTMC(TMC_Registers reg, int N, uint16_t *data) {
    int ret;
    ret = modbus_write_registers(TMC, reg, N, data);
    //modbus_flush(TMC);

    if (ret == -1) {
        if (throw_if_error)
            throw tread_exception(modbus_strerror(errno));
        else
        {
          fprintf(stderr, "%s\n", modbus_strerror(errno));
          return -1;
        }
    }

    return 0;
}

/* FUNCTION writeTMC(TMC_Registers reg, uint16_t *data)
 * Read a byte from a register
 * @PARAM reg - Register to write the data to
 * @PARAM data - Unsigned integer data to be written to register
 * @RETURN int = -1 if error, 0 if ok
 */
int tread_communicator::readTMC(TMC_Registers reg, int N, uint16_t *data) {
    int ret;
    modbus_flush(TMC);
    ret = modbus_read_registers(TMC, reg, N, data);

    if (ret == -1) {
        if (throw_if_error)
            throw tread_exception(modbus_strerror(errno));
        else
        {
          fprintf(stderr, "%s\n", modbus_strerror(errno));
          return -1;
        }
    }

    return 0;
}

/* FUNCTION openTMC()
 * Opens the treadmill device.
 * @RETURN int = -1 if error, 0 if ok
 */
int tread_communicator::openTMC() {
    int ret;
    std::cout << "Opening treadmill device" << std::endl;

    // Create the serial device
    TMC = modbus_new_rtu(devloc, baudrate, 'N', 8, 1);
    if (TMC == NULL) {
       modbus_close(TMC);
       modbus_free(TMC);
       TMC = NULL;
       if (throw_if_error)
           throw tread_exception(std::string("Connection failed: ") + modbus_strerror(errno));
       else
           fprintf(stderr, "Connection failed: %s\n", modbus_strerror(errno));
       return -1;
    }

    // DEBUG OPTIONS - COMMENT OUT IF NOT DEBUGGING!
    //modbus_set_debug(TMC, TRUE);

    // Set timeout
    uint32_t response_timeout_sec;
    uint32_t response_timeout_usec;
    /* Define a new timeout! */
    response_timeout_sec = 0;
    response_timeout_usec = 50000;
    modbus_set_response_timeout(TMC, response_timeout_sec, response_timeout_usec);

     // Set the slave address
     ret = modbus_set_slave(TMC, 0x1F);
     if (ret == -1) {
        modbus_close(TMC);
        modbus_free(TMC);
        TMC = NULL;
        if (throw_if_error)
            throw tread_exception(std::string("Connection failed: ") + modbus_strerror(errno));
        else
            fprintf(stderr, "Connection failed: %s\n", modbus_strerror(errno));
        return -1;
     }

     // SET SERIAL MODE TO RS485
     modbus_rtu_set_serial_mode(TMC, MODBUS_RTU_RS485);
     // Set RTS MODE
     modbus_rtu_set_rts(TMC, MODBUS_RTU_RTS_UP);


     // Connect to the modbus
     if (modbus_connect(TMC) == -1) {
         modbus_free(TMC);
         TMC = NULL;
         if (throw_if_error)
             throw tread_exception(std::string("Connection failed: ") + modbus_strerror(errno));
         else
             fprintf(stderr, "Connection failed: %s\n", modbus_strerror(errno));
         return -1;
     }
     //modbus_flush(TMC);

     return 0;
}

/* FUNCTION closeTMC()
 * Closes the treadmill device properly.
 */
int tread_communicator::closeTMC() {
    modbus_close(TMC);
    modbus_free(TMC);
    TMC = NULL;

    return 0;
}

/* FUNCTION speed_map(double robot_velocity)
 * Convert robot forward velocity to treadmill motor frequency.
 * To get experimental radius do testing to calculate motor radius:
 *      Rad = (d / t) / (2*pi*f)
 *          d = travel distance
 *          t = time
 *          f = commanded frequency
 * Current experimental radius -- Rad = 0.012631344689833 meters
 * @PARAM robot_velocity - Forward velocity in m/s of robot.
 * @RETURN motor_speed - Frequency in Hz of motor - cast as integer.
 */
int tread_communicator::speed_map(double robot_velocity) {
    int motor_speed = (int) (robot_velocity * 6.0 * 100.);
    return motor_speed; // return motor speed in hz
}

double tread_communicator::speed_map_inverse(int freq)
{
    double speed = freq / (6.0 * 100.);
    return speed;
}


