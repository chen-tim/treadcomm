#include <stdio.h>
#include <tread_comm/tread_comm.h>
#include <tread_comm/tread_srv.h>
#include <math.h>
#include <unistd.h>
#include <ros/ros.h>

using namespace std;

int status;
float speed;

bool tread_speed(tread_comm::tread_srv::Request  &req,
                 tread_comm::tread_srv::Response  &res)
{
  tread_communicator tempdrive("/0", 0);

  status = req.treadmill_status;
  speed = req.treadmill_speed;

  res.hz = tempdrive.speed_map(req.treadmill_speed);

  ROS_INFO("Set treadmill status to: \t %ld",   req.treadmill_status);
  ROS_INFO("Set treadmill speed to: \t %f m/s", req.treadmill_speed);
  ROS_INFO("Set treadmill speed to: \t %d Hz",  (int)res.hz);

  return true;
}


int main(int argc, char **argv)
{
  ros::init(argc, argv, "tread_comm_server");
  ros::NodeHandle nh;
  ros::ServiceServer service = nh.advertiseService("tread_set_speed", tread_speed);

  const char *device = "/dev/ttyUSB0";
  tread_communicator tDriver(device, 19200);
  speed = 0.0; // 1m/s
  status = 1;

  double sec;
  u_int32_t p_sec = 0;
  u_int32_t p_usec = 500000;
  modbus_get_response_timeout(tDriver.TMC, &p_sec, &p_usec);
  sec = p_sec + p_usec / 1.e6;
  cout << "timeout: " << sec << "\n";

  tDriver.openTMC();
  tDriver.stop();
  usleep(500000);

  tDriver.startup();
  usleep(500000);

  // For testing command receive
  //testfreq = tDriver.getSpeed();
  //std::cout << testfreq << std::endl; // output first value of the return

  // For testing command
  //int ret = tDriver.setSpeed(0.2);
  //int ret = tDriver.setFrequency(400);
  //usleep(500000);

  while (ros::ok()) {
      ros::spinOnce();
      if (status != 1) {
          tDriver.setSpeed(0);
      } else {
        tDriver.setSpeed(speed);
      }
      usleep(500000);
  }
  tDriver.setSpeed(0);
  usleep(500000);
  tDriver.stop();
  tDriver.closeTMC();

  return 0;
}
